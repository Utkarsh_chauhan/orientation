# Git:
Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

![Non-linear workflow](https://miro.medium.com/max/4800/1*qSYzfDqtyL6ESBzvxatL4A.png)

## Important Terms:
**Repository:**
It is the collection of files and folders (code files) that you’re using git to track.

**Commit:**
The "commit" command is used to save your changes to the local repository.

**Push:**
Pushing is essentially syncing your commits to GitLab.

**Branch:**
A branch is like a parallel world where you can create commit without introducing bugs into production code.

**Merge:**
Merge integrates two branches. When a branch becomes error free and is ready to become part of main branch, it is merged.

**Clone:**
Clone means making an exact copy on local machine.

**Fork:**
A fork is a copy of a repository that allows you to freely experiment with changes without affecting the original project.

## How to install Git:
### For Linux :
Open the terminal and type: 


```
sudo apt-get install git
```

### For Windows :
[Download this installer and run it](https://git-scm.com/download/win)


## Git Internals:
  Basic Commands to move a file  |  Basic workflow
:-------------------------:|:-------------------------:
![](https://backlog.com/app/themes/backlog-child/assets/img/guides/git/basics/git_workflow_002.png)  |  ![](https://miro.medium.com/max/686/1*diRLm1S5hkVoh5qeArND0Q.png))




**There are mainly 4 stages:**



**Untracked** — You create a brand new file called names.txt in your project. The file is now in untracked state.

**Staged** — You execute command git add names.txt . Now the file is staged.

**Committed** — You execute commit -m "names file added" . Now the change is committed. Here, the “change” is the creation of the new file.

**Modified** — You add the text hello world to the file and save the file. Now the file is modified.

*When you execute git add names.txt the file will be staged and the cycle continues.*


## Git workflow:
This helps to use Git to accomplish work in a consistent and productive manner. Git workflows encourage users to leverage Git effectively and consistently. Git offers a lot of flexibility in how users manage changes. Below image explains in brief the git workflow.
![git_workflow](https://i.stack.imgur.com/vPiEW.png)


## Git Commands:
There are some basic commands that one should be aware of, few of them are:


  Essential Commands  |  Commands and its workflow
:-------------------------:|:-------------------------:
![](https://www.tutsmake.com/wp-content/uploads/2020/01/Essential-git-commands-every-developer-should-know.jpeg)  |  ![](https://miro.medium.com/max/500/1*nCW0esK4O3OiP0HFKqLS4Q.png))



---
### Connect with me:

[<img align="left" alt="codeSTACKr | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />](https://www.linkedin.com/in/utkarshchauhan2629)

[<img align="left" alt="codeSTACKr | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />](http://www.instagram.com/utkarsh.chauhan__)

<br>

---
